provider "google" {
    credentials         = "${file("${var.credentials_file}")}"
    project             = "${var.project_id}"
    region              = "${var.region}"
}

terraform {
  backend "gcs" {
    credentials         = "credentials/account.json"
  }
}

data "terraform_remote_state" "trumeter" {
  backend = "gcs"
  config {
    bucket              = "${var.bucket}"
    path                = "${var.state_path}"
    project             = "${var.project_id}"
    credentials         = "${file("${var.credentials_file}")}"
  }
}

data "google_dns_managed_zone" "zone" {
  name = "${var.managed_zone_name}"
}

module "vpc" {
  source                    = "modules/vpc"
  name                      = "${var.vpc_name}"
  region                    = "${var.region}"
  routing_mode              = "${var.routing_mode}"
  public_subnet_cidr_range  = "${var.public_subnet_cidr_range}"
  private_subnet_cidr_range = "${var.private_subnet_cidr_range}"
}

module "cluster" {
  source                      = "modules/gke"
  name                        = "${var.cluster_name}"
  region                      = "${var.region}"

  node_count                  = "${var.cluster_node_count}"
  network                     = "${module.vpc.network}"
  subnetwork                  = "${module.vpc.private_subnetwork}"

  min_master_version          = "${var.min_master_version}"
  enable_kubernetes_dashboard = "${var.enable_kubernetes_dashboard}"

  machine_type                = "${var.machine_type}"
  min_node_count              = "${var.min_node_count}"
  max_node_count              = "${var.max_node_count}"
  oauth_scopes                = "${var.oauth_scopes}"
  disk_size_gb                = "${var.disk_size_gb}"
  disk_type                   = "${var.disk_type}"
}

module "web_traffic_firewall" {
  source                      = "modules/firewall"
  name                        = "${var.cluster_name}-web"
  network                     = "${module.vpc.network}"
  protocol                    = "tcp"
  ports                       = ["80","443"]
  source_ranges               = ["0.0.0.0/0"]
  target_tags                 = ["${var.cluster_name}-server"]
}

module "internal_traffic_firewall" {
  source                      = "modules/firewall"
  name                        = "${var.cluster_name}-internal"
  network                     = "${module.vpc.network}"
  source_ranges               = ["${var.cluster_cidr_range}"]
  enable_internal_firewall    = "true"
}

module "ip" {
  source                      = "modules/regional_address"
  name                        = "${var.cluster_name}"
}

module "dns" {
  source                      = "modules/dns"
  name                        = "${data.google_dns_managed_zone.zone.dns_name}"
  managed_zone_name           = "${data.google_dns_managed_zone.zone.name}"
  record_type                 = "A"
  ttl                         = "${var.ttl}"
  ips                         = ["${module.ip.address}"]
  records                     = "${var.records}"
}

module "database_instance" {
  source                      = "modules/database_instance"
  name                        = "trumeter-database-instance"
  database_version            = "MYSQL_5_7"
  region                      = "${var.region}"
  project_id                  = "${var.project_id}"

  db_tier                     = "db-n1-standard-1"

  disk_autoresize             = true
  disk_size                   = 100
  disk_type                   = "PD_SSD"

  backup_time                 = "00:00"

  ipv4_enabled                = true
  authorized_networks         = [{
                                  name = "all"
                                  value = "0.0.0.0/0"
                                }]
}

module "support_database" {
  source                      = "modules/database"
  name                        = "pre-production-trumeter"
  instance                    = "${module.database_instance.name}"
  charset                     = "utf8"
  collation                   = "utf8_general_ci"
}

module "ewsc_database" {
  source                      = "modules/database"
  name                        = "ewsc-production-trumeter"
  instance                    = "${module.database_instance.name}"
  charset                     = "utf8"
  collation                   = "utf8_general_ci"
}