variable "project_id" {
  type = "string"
}

variable "region" {
  type = "string"
  description = "Examples: us-central1, us-east1, europe-north1. More here: https://cloud.google.com/compute/docs/regions-zones/"
}

variable "bucket" {
    type = "string"
    description = "Google Storage bucket storing terraform state file"
}

variable "state_path" {
    type = "string"
    description = "Path to terraform state file"
    default = "state/single-tenant/terraform.tfstate"
}

variable "credentials_file" {
    type = "string"
    description = "Path to local service account credentials file"
    default = "credentials/account.json"
}

variable "vpc_name" {
    type = "string"
    description = "VPC network name"
}


variable "routing_mode" {
    type = "string"
    description = "Network routing mode. Options: REGIONAL and GLOBAL"
}

variable "cluster_cidr_range" {
    type = "string"
    description = "Cluster network cidr range"
}


variable "public_subnet_cidr_range" {
    type = "string"
    description = "Range of internal addresses owned by the subnet"
}

variable "private_subnet_cidr_range" {
    type = "string"
    description = "Range of internal addresses owned by the subnet"
}

variable "cluster_name" {
    type = "string"
    description = "Cluster name"
}

variable "cluster_node_count" {
    type = "string"
    description = "Initial number of nodes in to be created in the cluster"
}

variable "min_master_version" {
    type = "string"
    description = "Minimum kubernetes version"
    default = "1.12"
}

variable "enable_kubernetes_dashboard" {
    type = "string"
    description = "true to diable and false to enable. Enabled by default."
    default = false
}

variable "machine_type" {
    type = "string"
    description = "Machine type used by thre node pool"
    default = "n1-standard-1"
}

variable "min_node_count" {
    type = "string"
    description = "Minimum node count used in autoscaling"
}

variable "max_node_count" {
    type = "string"
    description = "Maximum nodes auto_scaling can scale to"
}

variable "oauth_scopes" {
    type = "list"
    description = "OAuth scopes that let the nodes access storage, log and monitor"
}

variable "disk_size_gb" {
    type = "string"
    description = "Disk size of nodes in GB e.g. 10GB >= disk_size_bg <= 100GB"
    default = 100
}

variable "disk_type" {
    type = "string"
    description = "Disk type e.g. pd-ssd, pd-standard"
    default = "pd-standard"
}

variable "managed_zone_name" {
    type = "string"
    description = "DNS Zone name"
}

variable "ttl" {
    type = "string"
    description = "TTL of the DNS record"
    default = 300
}

variable "records" {
    type = "list"
    description = "List of DNS records values i.e [subdomain, subdomain1]"
}
