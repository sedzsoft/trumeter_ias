
output "name" {
  value = "${var.name}"
}

output "database" {
  value = "${google_sql_database.this.self_link}"
}

output "db_username" {
  value = "${google_sql_user.db-user.name}"
}

output "db_password" {
  value = "${google_sql_user.db-user.password}"
}
