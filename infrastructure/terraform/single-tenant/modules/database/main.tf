resource "google_sql_database" "this" {
    name = "${var.name}"
    instance = "${var.instance}"
    charset = "${var.charset}"
    collation = "${var.collation}"
}

resource "random_id" "db-user" {
    byte_length = 8
}

resource "random_id" "db-password" {
    byte_length = 16
}

resource "google_sql_user" "db-user" {
    name = "${random_id.db-user.b64}"
    instance = "${var.instance}"
    password = "${random_id.db-password.b64}"
    host = ""
}
