variable "name" {
    type = "string"
    description = "SQL database name"
}

variable "instance" {
    type = "string"
    description = "Name of database instance where the database will be deployed."
}

variable "charset" {
    type = "string"
    description = "Character set i.e. utf8, latin1"
    default = "utf8"
}

variable "collation" {
    type = "string"
    default = "utf8_general_ci"
}
