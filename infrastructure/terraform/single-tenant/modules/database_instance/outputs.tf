output "name" {
  value = "${google_sql_database_instance.this.name}"
}

output "instance" {
  value = "${google_sql_database_instance.this.self_link}"
}

output "public_ip" {
  value = "${google_sql_database_instance.this.public_ip_address}"
}

output "private_ip" {
  value = "${google_sql_database_instance.this.private_ip_address}"
}

