variable "name" {
    type = "string"
    description = "Database instance name"
}

variable "database_version" {
    type = "string"
    description = "Databse type and version i.e. MYSQL_5_7, POSTGRES_11"
}

variable "region" {
    type = "string"
    description = "Region to locate the database instance i.e. us_central1"
}

variable "project_id" {
    type = "string"
    description = "Project ID for billing purposes"
}

variable "db_tier" {
    type = "string"
    description = "Database tier to determine the resources the dtabase instance will have i.e. RAM, storage"
}

variable "disk_autoresize" {
    type = "string"
    description = "Specifies if the storage should be increased automatically. Default: true"
    default = true
}

variable "disk_size" {
    type = "string"
    description = "Size of data disk. Default: 10GB"
    default = 10
}

variable "disk_type" {
    type = "string"
    description = "Type of data disk i.e. PD_SSD and PD_HDD"
    default = "PD_SSD"
}

variable "backup_time" {
    type = "string"
    description = "Time when backup should commence. Format: HH:MM"
}

variable "ipv4_enabled" {
    type = "string"
    description = "Specifies if databse instance should be given a public IP."
}

variable "authorized_networks" {
    type = "list"
    description = "Names CIDR for networks allowed to access the database instance i.e. authorized_networks = [{name = 'all' value = '0.0.0.0/0'}]"
}
