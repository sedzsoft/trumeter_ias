resource "random_string" "instance_suffix" {
    length = 4
    special = false
}

locals {
    instance_suffix = "${lower("${random_string.instance_suffix.result}")}"
}


resource "google_sql_database_instance" "this" {
    name = "${var.name}-${local.instance_suffix}"
    database_version = "${var.database_version}"
    region = "${var.region}"
    project = "${var.project_id}"

    settings {
        tier = "${var.db_tier}"
        
        disk_autoresize = "${var.disk_autoresize}"
        disk_size = "${var.disk_size}"
        disk_type = "${var.disk_type}"

        backup_configuration {
            binary_log_enabled = true
            enabled = true
            start_time = "${var.backup_time}"
        }

        ip_configuration {
            ipv4_enabled = "${var.ipv4_enabled}"

            authorized_networks = "${var.authorized_networks}"
        }
    }
}
