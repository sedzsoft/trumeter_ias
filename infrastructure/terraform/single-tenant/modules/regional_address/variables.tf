variable "name" {
    type = "string"
    description = "Regional IP address name"
}