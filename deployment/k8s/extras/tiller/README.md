## Setting-up tiller

### Minikube

```
kubectl apply -f extras/tiller/rbac.yaml
helm init --service-account tiller
helm init --upgrade --service-account tiller
```